open Printf

let f = lazy (open_out_gen [Open_text;Open_creat;Open_append;Open_wronly] 0o640 "C:\\ocaml.log")
let pr = fprintf (Lazy.force f) "%s\n%!"
let should_stop = ref false

module S = struct
  let name = "A_OCAML_SERVICE"
  let display = "A_OCAML_SERVICE"
  let text = "Test service written in OCaml"
  let arguments = []
  let stop () = should_stop := true
end

module Svc = Service.Make(S)

let main () =
  pr "main";
  Gc.compact ();
  pr "running";
  while not !should_stop do
   let () = Unix.sleep 1 in
   pr "LOOP"
  done;
  pr "finished"

let () =
  match List.tl (Array.to_list Sys.argv) with
  | ["-install"] -> Svc.install (); printf "Installed %S" S.name
  | ["-remove"] -> Svc.remove (); printf "Removed %S" S.name
  | ["-start"] -> Svc.start (); printf "Started from exec"
  | [] -> begin try Svc.run main with exn -> pr (Printexc.to_string exn) end
  | _ -> print_endline "doing nothing"
