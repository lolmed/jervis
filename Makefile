.PHONY: default build install uninstall test clean

default: build

build:
	dune build

test:
	dune runtest -f

install:
	dune install

uninstall:
	dune uninstall

clean:
	dune clean
# Optionally, remove all files/folders ignored by git as defined
# in .gitignore (-X).
	git clean -dfXq

deps:
	# opam pin add -yn jervis . && opam install --deps-only jervis
	opam install --deps-only jervis


cross:
	docker build -t ocamlcross .
	docker rm ocamlcross-build || echo 'ok'
	docker create --rm --name ocamlcross-build ocamlcross
	docker cp ocamlcross-build:/home/opam/helloworld.exe helloworld.exe
lol:
	$(CXX) main.cc -o lol.exe

