let rec loop () =
  let () = print_endline "Loop!" in
  let%lwt () = Lwt_unix.sleep 0.5 in
  loop ()

let () =
  Lwt_main.run (loop ())
