FROM dockcross/windows-x64

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
# RUN apt-get install -y \
#   gcc-mingw-w64-i686 \
#   gcc-mingw-w64-x86-64 \
#   gcc-multilib \
#   m4 \
#   xorg \
#   xorg-dev \
#   libx11-dev \
#   gawk \
#   aspcud \
#   libpcre3-dev \
#   libssl-dev

# RUN apt-get install -y libgmp3-dev
# RUN apt-get install -y mingw32

RUN useradd -g staff --create-home opam
USER opam

RUN curl -kL https://raw.github.com/hcarty/ocamlbrew/master/ocamlbrew-install | env CC=gcc OCAMLBREW_FLAGS="-r" bash

ENV PATH=/home/opam/ocamlbrew/ocaml-4.05.0/bin:$PATH

RUN opam init --auto --compiler=4.07.0
RUN opam switch 4.07.0 && eval $(opam config env) && opam install -y lwt lwt_ppx
 

RUN eval $(opam config env) && opam repository add windows git://github.com/ocaml-cross/opam-cross-windows
USER root
RUN apt-get install -y \
  gcc-mingw-w64-x86-64 \
  m4 \
  gawk \
  aspcud \
  libpcre3-dev \
  libssl-dev \
  libgmp3-dev \
  mingw32

USER opam

RUN eval $(opam config env) && opam update && opam install -y ocaml-windows
RUN eval $(opam config env) && opam install -y lwt-windows.3.2.1 lwt_ppx

WORKDIR /home/opam

# XXX
# copy linux runnable lwt_ppx to the windows path...
# XXX
RUN cp /home/opam/.opam/4.07.0/lib/lwt_ppx/ppx.exe /home/opam/.opam/4.07.0/windows-sysroot/lib/lwt/ppx.exe 

ADD --chown=opam:staff . jervis
WORKDIR /home/opam/jervis

RUN ls -ltra

# RUN eval $(opam config env) && ocamlfind -toolchain windows ocamlopt hello_lwt_ppx.ml -o hello_lwt_ppx.exe -linkpkg -package lwt.ppx,lwt.unix
RUN eval $(opam config env) && opam pin add -yn jervis .
RUN eval $(opam config env) && opam install --deps-only jervis
ENV TOOLPREF64=/usr/src/mxe/usr/bin/x86_64-w64-mingw32.static-
# RUN make lol
# RUN ls
# RUN wine lol.exe
# USER root
# RUN cd / && find . -name windows.h
# USER opam
RUN eval $(opam config env) && dune build

# bin/dun
  # (libraries cryptokit lwt lwt.unix cohttp cohttp-lwt-unix yojson)
